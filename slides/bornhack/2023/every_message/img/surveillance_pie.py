from matplotlib import pyplot as plt

"""
fig, ax = plt.subplots()
ax.pie((99,1), explode=(0,1), labels=('lawful content', 'CSAM'),
        shadow=True, startangle=90)
"""

labels = 'Not CSAM', 'CSAM',
sizes = 99.7, 0.3,
explode = 0, 0.15,

fig, ax = plt.subplots()
ax.pie(sizes, explode=explode, labels=labels,
        shadow=True, startangle=330)

plt.savefig('cake.png')
